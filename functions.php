<?php

$loader = require __DIR__ . '/vendor/autoload.php';

/**
 * Additional features to allow styling of the templates.
 */
require get_parent_theme_file_path( '/includes/template-functions.php' );

Includes\Core::init();

Includes\App::init();
