<?php

namespace Includes;

class App {
  public static function init () {
    $staff = new \ContentTypes\Staff();
    $staff->register();

    $ajax = new \ContentTypes\Ajax();
    $ajax->setup();
  }
}
