<?php

namespace Includes;

class Core {
  public static function init () {
    require_once 'constants.php';

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    // Register Sidebars
    $sidebars = new \ContentTypes\Sidebar();

    // Register Widgets
    $widgets = new \ContentTypes\Widgets();

    add_action('init', array( '\ContentTypes\Shortcodes', 'setup' ));

    // REMOVE WP EMOJI
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_print_styles', 'print_emoji_styles');

    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );

    // Add theme support for Custom Logo.
  	add_theme_support( 'custom-logo', array(
  		'width'       => 250,
  		'height'      => 250,
  		'flex-width'  => true,
  	) );

    /**
    * Setup ACF
    * Add options pages
    */
    Classes\Scripts::setup();

    /**
    * Setup CMB2
    * Add options pages
    */
    Classes\CMB2::init();

    /**
    * WP Images
    * Add images sizes
    * Add theme support
    */
    Classes\Images::setup();

    /**
    * WP Navigation
    * Register Navigation Menus
    * Add current nav class action
    */
    Classes\Nav::setup();

    /**
    * WP Navigation
    * Register Navigation Menus
    * Add current nav class action
    */
    Classes\Map::setup();

    Classes\Customizer::setup();

  }
}
