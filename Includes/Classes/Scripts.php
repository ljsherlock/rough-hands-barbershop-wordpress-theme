<?php

namespace Includes\Classes;

class Scripts {

  public static function setup () {
      add_action('wp_enqueue_scripts', array(__CLASS__, 'load_scripts'));
  }

  /**
  *  Register & Load CSS & Scripts
  */
  public static function load_scripts () {
    wp_register_script('roughhands_home', get_template_directory_uri() . '/dist/bundle.home.js', array(), date("H:i:s"), true );
    wp_register_script('roughhands_main', get_template_directory_uri() . '/dist/bundle.main.js', array(), date("H:i:s"), true );

    if(is_front_page()){
        wp_enqueue_style( 'roughhands_home_style', get_template_directory_uri() . '/dist/css/home.css', array(), date("H:i:s"));
        wp_enqueue_script('roughhands_home');
    }

    wp_enqueue_style( 'themename_main_style', get_template_directory_uri() . '/dist/css/main.css', array(), date("H:i:s"));
    wp_enqueue_script('roughhands_main');
  }
}
