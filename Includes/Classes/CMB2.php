<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'roughhands_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category Theme
 */

namespace Includes\Classes;

class CMB2 {
    public static $prefix = '_roughhands_meta_';

    public static function init () {
      if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
      	require_once dirname( __FILE__ ) . '/cmb2/init.php';
      } elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
      	require_once dirname( __FILE__ ) . '/CMB2/init.php';
      }

      add_action( 'cmb2_admin_init', array( __CLASS__, 'roughhands_register_front_page_meta') );
      add_action( 'cmb2_admin_init', array( __CLASS__, 'roughhands_register_theme_options_metabox') );
      add_action( 'cmb2_admin_init', array( __CLASS__, 'roughhands_register_staff_meta') );
    }

   /**
   * Front Page
   * Hook in and add a metaboxes that only appears for the 'Project' posts
   * @return
   * @
   */
    public static function roughhands_register_front_page_meta () {

      /**
      * Meta fields for the header call-to-action
      */

      $cmbHomepageCallToActionBox = new_cmb2_box( array(
          'id'           => self::$prefix . 'HomepageCallToActionBox',
          'title'        => esc_html__( 'Call to Action Meta', 'cmb2' ),
          'object_types' => array( 'page' ), // Post type
          'context'      => 'normal',
          'priority'     => 'high',
          'show_names'   => true, // Show field names on the left
          'show_on'      => array(
              'id' => array( 6 ),
          ), // Specific post IDs to display this metabox
      ) );

      $cmbHomepageCallToActionBox->add_field(array(
        'name' => esc_html__( 'Link', 'cmb2' ),
        'id'   => self::$prefix . 'headerCtalink',
        'type' => 'text',
      ) );

      $cmbHomepageCallToActionBox->add_field(array(
        'name' => esc_html__( 'Text', 'cmb2' ),
        'id'   => self::$prefix . 'headerCtaText',
        'type' => 'text',
      ) );

      /**
      * Meta fields for the 'What We Do section' (made up title as it doesn't have one).
      */

      $cmbWhatWeDoBox = new_cmb2_box( array(
          'id'           => self::$prefix . 'WhatWeDoBox',
          'title'        => esc_html__( 'What We Do Meta', 'cmb2' ),
          'object_types' => array( 'page' ), // Post type
          'context'      => 'normal',
          'priority'     => 'high',
          'show_names'   => true, // Show field names on the left
          'show_on'      => array(
              'id' => array( 6 ),
          ), // Specific post IDs to display this metabox
      ) );

      $cmbWhatWeDo = $cmbWhatWeDoBox->add_field(array(
          'id'   => self::$prefix . 'whatWeDoSection',
          'type' => 'group',
          'options'     => array(
            'group_title'   => __( 'block {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
            'add_button'    => __( 'Add Another block', 'cmb2' ),
            'remove_button' => __( 'Remove block', 'cmb2' ),
            // 'closed'     => true, // true to have the groups closed by default
          ),
      ));

      $cmbWhatWeDoBox->add_group_field($cmbWhatWeDo, array(
        'name' => esc_html__( 'Icon', 'cmb2' ),
        'id'   =>  'icon',
        'type' => 'file',
      ) );

      $cmbWhatWeDoBox->add_group_field($cmbWhatWeDo, array(
        'name' => esc_html__( 'Text', 'cmb2' ),
        'id'   =>  'title',
        'type' => 'text',
      ) );

      $cmbWhatWeDoBox->add_group_field($cmbWhatWeDo, array(
        'name' => esc_html__( 'Text', 'cmb2' ),
        'id'   =>  'content',
        'type' => 'textarea',
      ) );

      /**
      * Meta fields for the 'Proud at What We Do' slider
      */
      $cmbProudAtWhatWeDoBox = new_cmb2_box( array(
          'id'           => self::$prefix . 'ProudAtWhatWeDoBox',
          'title'        => esc_html__( 'Proud of Meta', 'cmb2' ),
          'object_types' => array( 'page' ), // Post type
          'context'      => 'normal',
          'priority'     => 'high',
          'show_names'   => true, // Show field names on the left
          'show_on'      => array(
              'id' => array( 120 ),
          ), // Specific post IDs to display this metabox
      ) );

      $cmbProudAtWhatWeDoSlider = $cmbProudAtWhatWeDoBox->add_field(array(
          'id'   => self::$prefix . 'proudAtWhatWeDoSlider',
          'type' => 'group',
          'options'     => array(
            'group_title'   => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
            'add_button'    => __( 'Add Another Image', 'cmb2' ),
            'remove_button' => __( 'Remove Image', 'cmb2' ),
            // 'closed'     => true, // true to have the groups closed by default
          ),
      ));

      $cmbProudAtWhatWeDoBox->add_group_field($cmbProudAtWhatWeDoSlider, array(
        'name' => esc_html__( 'Image', 'cmb2' ),
        'id'   =>  'image',
        'type' => 'file',
      ) );

    }

    /**
    * Front Page
    * Hook in and add a metaboxes that only appears for the 'Project' posts
    * @return
    * @
    */
     public static function roughhands_register_staff_meta () {
       /**
       * Meta fields for the header call-to-action
       */
       $cmb_staff = new_cmb2_box( array(
           'id'           => self::$prefix . 'staff',
           'title'        => esc_html__( 'Staff Meta', 'cmb2' ),
           'object_types' => array( 'staff' ), // Post type
           'context'      => 'normal',
           'priority'     => 'high',
           'show_names'   => true, // Show field names on the left
       ) );

       $cmb_staff->add_field( array(
         'name' => esc_html__( 'Years Experience', 'cmb2' ),
         'id'   => self::$prefix . 'yearsExp',
         'type' => 'text',
       ) );

       $cmb_staff->add_field( array(
         'name' => esc_html__( 'Email', 'cmb2' ),
         'id'   => self::$prefix . 'emailAddress',
         'type' => 'text',
       ) );
     }

    /**
    * Theme Options
    *
    * Hook in and register a metabox to handle a theme options page and adds a menu item.
    */
    public static function roughhands_register_theme_options_metabox () {

      /**
      *
      * Company Details
      *
      */
      $cmb_theme_settings_company = new_cmb2_box( array(
      'id'           => 'options_company',
      'title'        => esc_html__( 'Company Details', 'myprefix' ),
      'object_types' => array( 'options-page' ),
      /*
      * The following parameters are specific to the options-page box
      * Several of these parameters are passed along to add_menu_page()/add_submenu_page().
      */
      'option_key'      => 'settings-company', // The option key and admin menu page slug.
      'icon_url'        => 'dashicons-businessman', // Menu icon. Only applicable if 'parent_slug' is left empty.
      // 'menu_title'      => esc_html__( 'Options', 'myprefix' ), // Falls back to 'title' (above).
      // 'parent_slug'     => 'options-general.php', // Make options page a submenu item of the themes menu.
      'capability'      => 'manage_options', // Cap required to view options-page.
      // 'position'        => 2, // Menu position. Only applicable if 'parent_slug' is left empty.
      // 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
      // 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
      // 'save_button'     => esc_html__( 'Save Theme Options', 'myprefix' ), // The text for the options-page save button. Defaults to 'Save'.
      ));

      $cmb_theme_settings_company->add_field( array(
      'name'    => __( 'Company Name', 'cmb2' ),
      'id'      => self::$prefix . 'company_name',
      'type'    => 'text',
      ));

      $cmb_theme_settings_company->add_field( array(
      'name'    => __( 'Company Number', 'cmb2' ),
      'id'      => self::$prefix . 'company_number',
      'type'    => 'text',
      ));

      $cmb_theme_settings_company->add_field( array(
      'name'    => __( 'Registered Locations', 'cmb2' ),
      'id'      => self::$prefix . 'company_registered_locations',
      'type'    => 'text',
      ));

      $cmb_social_icons = $cmb_theme_settings_company->add_field( array(
      'name'    => __( 'Social Media Links ', 'cmb2' ),
      'id'      => self::$prefix . 'social_media_links',
      'type'        => 'group',
      // 'description' => __( 'Generates reusable form entries', 'cmb2' ),
      // 'repeatable'  => false, // use false if you want non-repeatable group
      'options'     => array(
      'group_title'   => __( 'Entry {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
      'add_button'    => __( 'Add Another Icon', 'cmb2' ),
      'remove_button' => __( 'Remove Icon', 'cmb2' ),
      // 'sortable'      => true, // beta
      // 'closed'     => true, // true to have the groups closed by default
      ),
      ));

      $cmb_theme_settings_company->add_group_field($cmb_social_icons, array(
      'name'    => __( 'Link ', 'cmb2' ),
      'id'      => 'link',
      'type'    => 'text',
      ));
      $cmb_theme_settings_company->add_group_field($cmb_social_icons, array(
      'name'    => __( 'Icon Slug ', 'cmb2' ),
      'id'      => 'icon',
      'type'    => 'text',
      ));

      /**
      * Contact Details
      */
      $cmb_theme_settings_contact = new_cmb2_box( array(
      'id'           => 'options_contact',
      'title'        => esc_html__( 'Contact Details', 'myprefix' ),
      'object_types' => array( 'options-page' ),
      /*
      * The following parameters are specific to the options-page box
      * Several of these parameters are passed along to add_menu_page()/add_submenu_page().
      */
      'option_key'      => 'settings-contact', // The option key and admin menu page slug.
      'icon_url'        => 'dashicons-phone', // Menu icon. Only applicable if 'parent_slug' is left empty.
      // 'menu_title'      => esc_html__( 'Options', 'myprefix' ), // Falls back to 'title' (above).
      // 'parent_slug'     => 'options-general.php', // Make options page a submenu item of the themes menu.
      // 'capability'      => 'manage_options', // Cap required to view options-page.
      // 'position'        => 3, // Menu position. Only applicable if 'parent_slug' is left empty.
      // 'admin_menu_hook' => 'network_admin_menu', // 'network_admin_menu' to add network-level options page.
      // 'display_cb'      => false, // Override the options-page form output (CMB2_Hookup::options_page_output()).
      // 'save_button'     => esc_html__( 'Save Theme Options', 'myprefix' ), // The text for the options-page save button. Defaults to 'Save'.
      ));

      $cmb_theme_settings_contact->add_field( array(
      'name'    => __( 'Contact Email ', 'cmb2' ),
      'id'      => self::$prefix . 'contact_email',
      'type'    => 'text_email',
      ));

      $cmb_theme_settings_contact->add_field( array(
        'name'    => __( 'Contact Telephone ', 'cmb2' ),
        'id'      => self::$prefix . 'contact_telephone',
        'type'    => 'text',
      ));

      $cmb_address = $cmb_theme_settings_contact->add_field( array(
      'name'    => __( 'Company Address ', 'cmb2' ),
      'id'      => self::$prefix . 'company_address',
      'type'        => 'group',
      'options'     => array(
      'group_title'   => __( 'Line {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
      'add_button'    => __( 'Add Another line', 'cmb2' ),
      'remove_button' => __( 'Remove line', 'cmb2' ),
      ),
      ));

      $cmb_theme_settings_contact->add_group_field($cmb_address, array(
      'name'    => __( 'Line ', 'cmb2' ),
      'id'      => 'line',
      'type'    => 'text',
      ));



    }

    /**
     * Wrapper function around cmb2_get_option
     * @since  0.1.0
     * @param  string $key     Options array key
     * @param  mixed  $default Optional default value
     * @return mixed           Option value
     */
    public static function roughhands_get_option($options_prefix, $key = '', $default = false ) {
    	if ( function_exists( 'cmb2_get_option' ) ) {
    		// Use cmb2_get_option as it passes through some key filters.
    		return cmb2_get_option( $options_prefix, $key, $default );
    	}
    	// Fallback to get_option if CMB2 is not loaded yet.
    	$opts = get_option( $options_prefix, $default );
    	$val = $default;
    	if ( 'all' == $key ) {
    		$val = $opts;
    	} elseif ( is_array( $opts ) && array_key_exists( $key, $opts ) && false !== $opts[ $key ] ) {
    		$val = $opts[ $key ];
    	}
    	return $val;
    }

    // get project posts with archive
    public static function get_project_options() {

      $options = array();

      $work = new \Controllers\Archive(array('query' => array(
        'post_type' => 'work'
      ) ));
      $posts = $work->returnData('posts');
      foreach($posts['postsArray'] as $post) {
        $options[$post->slug] = __( $post->title, 'cmb2' );
      }

      return $options;
    }

    // get project posts with archive
    public static function get_frontpage_options() {

      $options = array();

      $work = new \Controllers\Archive(array(
				'extra_data' => false,
				'query' => array(
		      'post_type' => array('post', 'work', 'page'),
		      'posts_per_page' => 100,
		      'orderby' => 'post_type',
	      // 'order' => 'descending'
       )));
      $posts = $work->returnData('posts');

      foreach($posts['postsArray'] as $post) {
        $options[$post->ID] = __( get_post_type_object($post->post_type)->labels->singular_name .' - ' . $post->title, 'cmb2' );
      }

      return $options;
    }

    public static function getFrontPageLinks($args) {
      $workTypeTerms = get_terms( $args );
      $array = [];

      foreach ($workTypeTerms as $key => $term) {
          $array[$term->term_id] = __( $term->taxonomy .' - '. $term->name, 'cmb2' );
      }

      return $array;
    }

    /**
     * Sample template tag function for outputting a cmb2 file_list
     *
     * @param  string  $file_list_meta_key The field meta key. ('wiki_test_file_list')
     * @param  string  $img_size           Size of image to show
     */
    public static function cmb2_output_file_list( $file_list, $img_size = 'thumbnail' ) {
      array_walk($file_list, function( &$attachment_url, $attachment_id, $img_size) {
        // Loop through them and output an image
        $attachment_url = wp_get_attachment_image( $attachment_id, $img_size );
      }, $img_size);

      return $file_list;
    }

    /**
    *	@method returnAllProducts
    *	@return Array of Product posts
    */
    public static function returnPostOptions( $args ) {
      $query = new \WP_Query( $args );
      $posts = $query->posts;

      if ( count($posts) > 0 ) {
        $postIDs = [];
        foreach ($posts as $key => $post) {
          $postIDs[$post->ID] = $post->post_title;
        }
        return $postIDs;
      } else {
        return null;
      }
    }

}
