<?php

namespace Includes\Classes;

class Images {
    public static function setup() {
      add_action('after_setup_theme', array(__CLASS__, 'add_image_sizes'));
    }


    /**
    * @param orignWidth
    * @param orignHeight
    */
    public static function percentageChange($originNumber, $newNumber) {
      return ($newNumber / $originNumber ) * 100;
    }

    /**
    * @param orignWidth
    * @param orignHeight
    * @param newWidth
    * @param newHeight
    */
    public static function resizeWH($orignWidth, $orignHeight, $newSize, $dimension) {

      if($dimension == 'width') {
        $percentageChange = self::percentageChange($orignWidth, $newSize);
        $newHeight = ($orignHeight * $percentageChange) / 100;
        $newWidth = $orignWidth;
      } else if($dimension == 'height') {
        $percentageChange = self::percentageChange($orignHeight, $newSize);
        $newWidth = ($originWidth * $percentageChange) / 100;
        $newHeight = $originHeight;
      }

      return array( (int)$newWidth, (int)$newHeight );
    }

    /**
     *  Add Image Sizes
     */
    public static function add_image_sizes () {
       add_theme_support('post-thumbnails');
    }

    /**
     *  Return image url based on Device
     *
     *  @param  {Array} Image Array
     *  @param  {String} image type
     *  @param  {boolean} Return Url Only
     *  @param  {String} Classes
     *  @example serve_image( $array, 'banner', true, false);
     */
     public static function serve_image( $image_array = array(), $type = 'tile', $srcOnly = false, $retina = false, $class = '' ) {
       $size = $image_array['sizes'];
       $title = $image_array['title'];
       $device = \Utils\Utils::isDevice();

       // key of image size iE tile-desktop
       $key = $type.'__'.$device;
       $key = ($retina) ? $key . '--retina' :  $key ;

       // Use the above key to get the value
       $src = $size[$key];

       // return the image as URL or IMG element
       return ( $srcOnly === true ) ? $src : '<img src="'. $src .'" alt="'. $title .'" class="'.$class.'"/>' ;
     }

     /**
     * Get all the registered image sizes along with their dimensions
     *
     * @global array $_wp_additional_image_sizes
     *
     * @link http://core.trac.wordpress.org/ticket/18947 Reference ticket
     * @return array $image_sizes The image sizes
     */
    public static function get_all_image_sizes($thumbnailID, $image_sizes = array( 'thumbnail', 'medium', 'large' )) {
    	// global $_wp_additional_image_sizes;

      $allSizes = get_intermediate_image_sizes();
      $matched_image_sizes = array();
      foreach ($image_sizes as $key => $size) {
        $matches = array_filter($allSizes, function($var) use ($size) { return preg_match("/\b$size\b/i", $var); });
        $matched_image_sizes = array_merge($matched_image_sizes, $matches);
      }


      // var_dump($matched_image_sizes);

      $images = array();
      $device = \Includes\Utils\Utils::isDevice();

      foreach ( $matched_image_sizes as $size ) {
          $images[$size] = array(
              'width'  => intval( get_option( "{$size}_size_w" ) ),
              'height' => intval( get_option( "{$size}_size_h" ) ),
              'crop'   => get_option( "{$size}_crop" ) ? get_option( "{$size}_crop" ) : false,
          );
      }

      // if ( isset( $_wp_additional_image_sizes ) && count( $_wp_additional_image_sizes ) ) {
      //     $images = array_merge( $images, $_wp_additional_image_sizes );
      // }

      foreach ( $matched_image_sizes as $size ) {
        $images[$size]['img'] = wp_get_attachment_image($thumbnailID, $size );
        $images[$size]['url'] = wp_get_attachment_url($thumbnailID, $size );
        // $media = strstr($size, '--', true);
        // $images[$media]['img'] = wp_get_attachment_image($thumbnailID, $media . '--' . $device );
        // $images[$media]['url'] = wp_get_attachment_url($thumbnailID, $media . '--' .$device );
      }

      // die(var_dump($images));

    	return $images;
    }

}
