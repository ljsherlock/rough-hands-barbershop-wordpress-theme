<?php

namespace Includes\Classes;

class Nav {
  public static function setup () {
    self::navSetup();
  }

  /**
  *  Register Navigation areas
  */
  public static function navSetup () {
    $navs = array(
      'primary' => 'Primary',
      'various' => 'Various',
      'social' => 'Social'
    );
    register_nav_menus( $navs );
  }
}
