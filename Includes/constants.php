<?php

// Wordpress Unique text domain
define('LJS_TEXT_DOMAIN', 'ljsherlock');

define('PROJECT_VERSION', '0.0.0');

// CUTTING THE MUSTARD
if( isset( $_COOKIE['cuts-the-mustard'] ) )
{
    define('_MUSTARD', $_COOKIE['cuts-the-mustard']);
} else {
    define('_MUSTARD', false);
}

// Base locations
define('LJS_ROOT', dirname(__FILE__));
define('LJS_URL', esc_url( home_url( '/' ) ) );
define('LJS_THEME_URL', get_stylesheet_directory_uri());

// Server side
// define('LJS_INC', LJS_ROOT . '/Includes');

//Client Side

define('LJS_AJAXURL', admin_url('admin-ajax.php'));

if (!defined('LJS_ASSET_LOADING_ENABLE_CACHE_BUSTING')) {
    define('LJS_ASSET_LOADING_ENABLE_CACHE_BUSTING', false);
}

if ( ! is_admin() ) {
    define('LJS_CACHE', 600);
}
