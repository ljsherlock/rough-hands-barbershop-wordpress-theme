# Rough Hands

## Description

This is a the build of a the frontpage as part of a custom theme for the Rough Hands barbershop

For a demo please go to [codeable.ljsherlock.com](codeable.ljsherlock.com)

Note: since this is a custom theme it takes more 'custom' data and settings to
setup, so I recommend going to the above link, as opposed to downloading and
installing this first.

## Installation

1. Clone this repository to your 'themes' folder
2. Run `npm install` to install javascript modules
3. In your admin panel, go to Appearance -> Themes
4. Click 'Activate' to use your new theme right away.
5. Navigate to Appearance > Customize in your admin panel and customize to taste.
