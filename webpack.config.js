
const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require('webpack');

module.exports = {
  entry: {
    main : "./src/index",
    home : "./src/components/templates/home/index"
  },
  output: {
      path: path.resolve(__dirname, "dist"),
      filename: "bundle.[name].js"
  },
  optimization: {
    splitChunks: {
      name: true,
    }
  },
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "css/[name].css",
      chunkFilename: "css/[contenthash].css",
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
              presets: ['env']
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
        MiniCssExtractPlugin.loader,
        {
          loader: "css-loader",
        },
        "resolve-url-loader",
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
            sourceMapContents: true
          }
        }
      ]},
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        loader: 'file-loader',
        options: {
          name: './font/[name].[ext]'
        }
      },
      {
        test: /\.(png|jp(e*)g|svg)$/,
        exclude: [
          path.resolve(__dirname, 'src/img/*'),
        ],
        include: [
          path.resolve(__dirname, 'src/img/lg'),
        ],
        use: [{
          loader: 'url-loader',
          options: {
            // limit: 8000, // Convert images < 8kb to base64 strings
            name: 'img/[hash].[ext]'

          }
        }]
      },
      {
        test: /\.(png|jp(e*)g|)$/,
        exclude: [
          path.resolve(__dirname, 'src/img/lg'),
        ],
        use: [{
          loader: 'file-loader',
          options: {
            // limit: 8000, // Convert images < 8kb to base64 strings
            name: 'img/[name].[ext]'
          }
        }]
      }
    ],
  }
}
