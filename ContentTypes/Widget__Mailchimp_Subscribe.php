<?php

namespace ContentTypes;

class Widget__Mailchimp_Subscribe extends Widget {
  public $name = 'Mailchimp Subscribe';
  public $className = 'mailchimp-subscribe';
  public $desc = "Mailchimp subscribe form";
  public $type = '';
  public $template = 'components/subscribeForm';

  /**
   * Registers the widget with the WordPress Widget API.
   *
   * @return void.
   */
  public static function register() {
    register_widget( __CLASS__ );
  }

  /**
  * Registers the widget with the WordPress Widget API.
  *
  * @return void.
  */
  public function __construct () {
      parent::__construct();
  }

  public function widget ($args, $instance) {
    parent::widget( $args, $instance );

    if (!empty($instance)) {
      // widget values
      $this->title = ( !empty( $instance['title'] ) ) ? $instance['title'] : __( 'Sign Up' );
      $this->actionAttr = ( !empty( $instance['actionAttr'] ) ) ? $instance['actionAttr'] : __( '' );
      $this->buttonText = ( !empty( $instance['buttonText'] ) ) ? $instance['buttonText'] : __( 'Subscribe' );
      $this->legal = ( !empty( $instance['legal'] ) ) ? $instance['legal'] : __( 'legal' );
      $this->type = 'widget';

      // initiate Controller
      $widget = new \Controllers\Widget__Mailchimp_Subscribe(
        array( 'widget' => $this, )
      );

      // Show the Widget
      $widget->show();
    }
  }

  public function update ($new_instance, $old_instance) {
    $instance = $old_instance;
    $instance['title'] = sanitize_text_field( $new_instance['title'] );
    $instance['actionAttr'] = sanitize_text_field( $new_instance['actionAttr'] );
    $instance['buttonText'] = sanitize_text_field( $new_instance['buttonText'] );
    $instance['legal'] = sanitize_text_field( $new_instance['legal'] );

    return $instance;
  }

  public function form ($instance) {
    $this->instances->title = new \stdClass();
    $this->instances->title->id = $this->get_field_id( 'title' );
    $this->instances->title->value = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
    $this->instances->title->name = $this->get_field_name( 'title' );
    $this->instances->title->title = 'Title:';

    $this->instances->actionAttr = new \stdClass();
    $this->instances->actionAttr->id = $this->get_field_id( 'actionAttr' );
    $this->instances->actionAttr->value = isset( $instance['actionAttr'] ) ? esc_attr( $instance['actionAttr'] ) : '';
    $this->instances->actionAttr->name = $this->get_field_name( 'actionAttr' );
    $this->instances->actionAttr->title = 'Form Action (HTML attribute):';

    $this->instances->buttonText = new \stdClass();
    $this->instances->buttonText->id = $this->get_field_id( 'buttonText' );
    $this->instances->buttonText->value = isset( $instance['buttonText'] ) ? esc_attr( $instance['buttonText'] ) : '';
    $this->instances->buttonText->name = $this->get_field_name( 'buttonText' );
    $this->instances->buttonText->title = 'Button Text:';

    $this->instances->legal = new \stdClass();
    $this->instances->legal->id = $this->get_field_id( 'legal' );
    $this->instances->legal->value = isset( $instance['legal'] ) ? esc_attr( $instance['legal'] ) : '';
    $this->instances->legal->name = $this->get_field_name( 'legal' );
    $this->instances->legal->title = 'Legal:';

    $this->type = 'form';

    //initiate Controller
    $widget = new \Controllers\Widget( array( 'widget' =>  $this ) );

    // Render the Form
    $widget->show();
  }
}
