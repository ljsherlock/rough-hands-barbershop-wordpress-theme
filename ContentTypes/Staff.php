<?php

namespace ContentTypes;

class Staff extends CustomPost
{
  protected $name = "staff";
  protected $singular = "Staff Member";
  protected $plural = "Staff";
  protected $icon = 'dashicons-groups';

  public function __construct() {

    parent::__construct();

    $this->args['supports'] = array( 'title', 'thumbnail', 'editor');

  }
}
