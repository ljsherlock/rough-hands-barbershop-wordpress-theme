class State {
  // For toggling the state with one button (on|off, 0|1)
  static toggleState(elem, one, two) {
    var elem = document.querySelector(elem)
    elem.setAttribute('data-state', elem.getAttribute('data-state') === one ? two : one)
  } // example: toggleState('.nav ul', 'closed', 'open')

  static setState(elem, state) {
    var elem = document.querySelector(elem)
    if (elem.getAttribute('data-state') !== state ) {
      elem.setAttribute('data-state', state)
    }
  } // example: setState('.nav ul', 'show')
}

export default State
