
var scrollElementToggle = (
  elementToggle = null,
  elementArea = null,
  scrolledToTopCallback = null,
  scrolledDownCallback = null,
  scrolledPastElemCallback = null,
  scrolledUpAboveElemCallback = null,
  scrolledUpCallback = null
) => {

  // We need to position the header above the window before we make it fixed so that
  // it can transition down

  let didScroll
  let lastScrollTop = 0
  const delta = 5
  let articleHeight = document.querySelector('.post-header').offsetHeight

  window.addEventListener("scroll",
    (event) => { didScroll = true }
  )

  setInterval(function() {
   if (didScroll) {
      hasScrolled()
      didScroll = false
   }
  }, 250)

  var hasScrolled = () => {
    let st = window.pageYOffset
    let header = document.querySelector('header')
    let body = document.querySelector('body')

    // Make surce they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
      return

    // Scrolled to the top
    if(st == 0) {    
     if (scrolledToTopCallback !== null) {
       scrolledToTopCallback(elementToggle)
     }

    // Scroll Down. So hide for better UX (reading)
    } else if (st > lastScrollTop && st > 0) {    
      if (scrolledDownCallback !== null) {
        scrolledDownCallback(elementToggle)
      }

      // Have we scrolled past the ""?
      if(st > articleHeight) {      
        if (scrolledPastElemCallback !== null) {
          scrolledPastElemCallback(elementToggle)
        }
      }

    // Scroll Up
    } else {    

      // If we're above the Elem Passed
      if(st < articleHeight) {      
        if (scrolledUpAboveElemCallback !== null) {
          scrolledUpAboveElemCallback(elementToggle)
        }
      } else {
        if (scrolledUpCallback !== null) {
          scrolledUpCallback(elementToggle)
        }
      }
    }
    lastScrollTop = st
  }
}

export default scrollElementToggle
