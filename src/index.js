
import './css/main.scss'
import './sass/styles.scss'
import './js/scripts.js'
import './components/parts/header-mobile/header-mobile.js'
import './components/parts/header/header.js'
import './components/parts/carousel/carousel.js'
import isRetina from './js/retina.js'

import homeIcon from './img/lg/logo_medium_white.png'
import homeMedium from './img/logo_small_white.png'
import logoTiny from './img/logo_tiny_white.png'

import staffDivider from './img/staff_divider.png'
import sectionDivider from './img/section_divider.png'
import nextPrevIcon from './img/next-prev.png'

import logoDesktopRetina from './img/lg/logo_long@2x.png'
import logoMDesktopSmallRetina from './img/lg/logo_medium_white@2x.png'

import logoSmallRetina from './img/lg/logo_small_white@2x.png'
import logoTinyRetina from './img/lg/logo_tiny_white@2x.png'
import staffDividerRetina from './img/lg/staff_divider@2x.png'
import sectionDividerRetina from './img/lg/section_divider@2x.png'
import iconToolsRetina from './img/lg/icn_tools@2x.png'
import nextPrevIconRetina from './img/lg/next-prev@2x.png'

document.getElementById('logoMobile').src = logoSmallRetina
document.getElementById('menuItemLogo').src = logoDesktopRetina
document.getElementById('menuItemLogoSmall').src = logoMDesktopSmallRetina
document.getElementById('LogoFooter').src = logoTinyRetina

document.querySelectorAll('.staff-divider').forEach(function (elem, index) {
  elem.src = staffDividerRetina
});
document.querySelectorAll('.section-divider').forEach(function (elem, index) {
  elem.src = sectionDividerRetina
});

// check if this is desktop
if (/Mobi|Android/i.test(navigator.userAgent)) {
  console.log('Mobile')
} else {

}

var retina = () => {
	if (!isRetina())
		return

  console.log('Is Retina')
	let hasRetina = $('img.has-retina')
  if (hasRetina.length > 0) {
    hasRetina.map(function(i, image) {
      var path = $(image).attr("src")
      path = path.replace(".png", "@2x.png")
      path = path.replace(".jpg", "@2x.jpg")
      $(image).attr("src", path)
    })
  }
}

$(document).ready(retina)

// var svgSupport = !!document.createElementNS &&
// !!document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect;
//
// if ( svgSupport ) {
//   document.querySelectorAll('.staff-divider').forEach(function (elem, index) {
//     elem.src = staffDividerSVG
//   });
//   document.querySelectorAll('.section-divider').forEach(function (elem, index) {
//     elem.src = sectionDividerSVG
//   });
// }
