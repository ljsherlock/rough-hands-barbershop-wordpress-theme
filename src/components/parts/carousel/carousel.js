
import 'owl.carousel';

$(document).ready(function() {
  $('.owl-carousel').owlCarousel({
    nav: true,
    dots: true,
    dotsEach: true,
    loop: true,
    navClass: ['owl-prev ml4 f3 outline-0 grow', 'owl-next mr4 f3 outline-0 grow'],
    dotsClass: 'owl-dots mw8 center bg-black-10 mt3 flex',
    responsive:{
      0:{
        items: 1,
        dots: true,
        dots: true,
      },
      460:{
        items: 2,
        dots: true,
        dots: true,
      },
      1024:{
        items: 3,
        dots: true,
        nav: true,
      }
    }
  })
})
