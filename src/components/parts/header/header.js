
import scrollElementToggle from '../../../js/scroll.js'
import State from '../../../js/state.js'

import logoDesktop from '../../../img/lg/logo_long.png'
import logoDesktopSmall from '../../../img/lg/logo_medium_white.png'

const desktopHeader = document.getElementById('primaryMenu')
const desktopLogo = document.getElementById('menuItemLogo')

desktopLogo.parentNode.innerHTML = `<img class="center db" id="menuItemLogo" style="height: 78px; width: auto;" src="${logoDesktop}">
  <img class="center dn mw5 pv2 h4" id="menuItemLogoSmall"  src="${logoDesktopSmall}">`

scrollElementToggle(
  '#primarySiteMenu',
  '.post-header',
  (elementToggle) => { State.setState(elementToggle, '') }, // scrolledToTopCallback
  null, // scrolledDownCallback
  (elementToggle) => { State.setState(elementToggle, 'hide') }, // scrolledPastElemCallback
  (elementToggle) => { State.setState(elementToggle, '') }, // scrolledUpAboveElemCallback
  (elementToggle) => { State.setState(elementToggle, 'show') }, // scrolledUpCallback
)
