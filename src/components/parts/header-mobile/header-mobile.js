
import scrollElementToggle from '../../../js/scroll.js'
import State from '../../../js/state.js'

let primaryHamburger = document.getElementById('primaryHamburger')
let primaryCloseHamburger = document.getElementById('primaryHamburgerClose')

scrollElementToggle(
  '#primarySiteMenuMobile',
  '.post-header',
  (elementToggle) => { State.setState(elementToggle, '') }, // scrolledToTopCallback
  null, // scrolledDownCallback
  (elementToggle) => { State.setState(elementToggle, 'hide') }, // scrolledPastElemCallback
  (elementToggle) => { State.setState(elementToggle, '') }, // scrolledUpAboveElemCallback
  (elementToggle) => { State.setState(elementToggle, 'show') }, // scrolledUpCallback
)

primaryHamburger.addEventListener('click', function(e) {
  State.setState('#primarySiteMenuModal', 'show')
  State.setState('html', 'no-scroll')
})

primaryCloseHamburger.addEventListener('click', function(e) {
  State.setState('#primarySiteMenuModal', 'hide')
  State.setState('html', '')
})
