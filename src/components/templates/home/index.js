/* Styles */
// import '../../../css/_normalize.css';
// import '../../../css/_typography.css';
// import '../../../css/_elements.css';
// import '../../../css/_navigation.css';
// import '../../../css/_accessibility.css';
// import '../../../css/_alignments.css';
// import '../../../css/_clearings.css';
// import '../../../css/_widgets.css';
// import '../../../css/_content.css';
// import '../../../css/_media.css';
// import '../../../css/_layout.css';
// import '../../../css/_global.css';

import './home-styles.scss'
import './home-scripts'

/* Components */
// import '../../parts/header/index'
// import '../../parts/footer/index'

/* Global Scripts */
import '../../../js/scripts'
