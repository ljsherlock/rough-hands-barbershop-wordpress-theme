<?php

namespace Models;

use Includes\Classes\CMB2 as CMB2;

class Staff extends Page {
  /**
  * __construct
  * @param array $args Model arguments
  */
  public function __construct( $args ) {
      parent::__construct( $args );
  }

  public function get() {

    // Get the staff posts
    $staffArchive = new \Controllers\Archive( array(
      'query' => array(
        'post_type' => 'staff'
      )
    ));

    $staffArchive->model->customThumbnailClasses = 'mw5';

    $staffPosts = $staffArchive->returnData('archivePosts');


    // Add it all to the timber context
    $this->timber->addContext( array(
      'staffPosts' => $staffPosts,
    ) );

    // Get the parent context (page, single (post thumbnails etc..))
    return parent::get();
  }
}
