<?php

namespace Models;

use Includes\Classes\CMB2 as CMB2;

class Frontpage extends Page {
  /**
  * __construct
  * @param array $args Model arguments
  */
  public function __construct( $args ) {
      parent::__construct( $args );
  }

  public function panelCount () {
    $panels = array( '1', '2', '3', '4' );
    $panel_count = 0;
    foreach ( $panels as $panel ) {
      if ( get_theme_mod( 'panel_' . $panel ) ) {
        $panel_count++;
      }
    }
    return $panel_count;
  }

  public function get() {

    /** We are going to use the twentyseventeen panels to create the sections
    * of the homepage.
    */
    $panels = array( '1', '2', '3', '4' );
		$titles = array();
		$twentyseventeencounter; // Used in components/page/content-front-page-panels.php file.
		if ( 0 !== $this->panelCount() || is_customize_preview() ) { // If we have pages to show.
			$twentyseventeencounter = 1;
			foreach ( $panels as $key => $panel ) {
				if ( get_theme_mod( 'panel_' . $panel ) ) {
					$postID = get_theme_mod( 'panel_' . $panel );
          switch ($postID) {
            case '87': // Staff
              $staff = new \Controllers\Staff( array(
                'id' => $postID,
                'standalone' => true,
                'query' => array('posts_per_page' => 3)
              ) );
              $panels[$key] = $staff->compile();
              break;
            case '91': // Products
              $page = new \Controllers\Products( array(
                'id' => $postID,
                'standalone' => true,
              'query' => array('posts_per_page' => 4)
              ) );
              $panels[$key] = $page->compile();
              break;
            case '120': // Products
              $page = new \Controllers\Carousel(array(
                'id' => $postID,
                'standalone' => true,
              ));
              $panels[$key] = $page->compile();
              break;
            // Simple page (default page.twig template)
            default:
              $page = new \Controllers\Page( array( 'id' => $postID, 'standalone' => true ) );
              $panels[$key] = $page->compile();
              break;
          }
				} else {
					// The output placeholder anchor.
          $panels[$key] = '';
					// echo '<article class="panel-placeholder panel twentyseventeen-panel twentyseventeen-panel' . esc_attr( $twentyseventeencounter ) . '" id="panel' . esc_attr( $twentyseventeencounter ) . '"><span class="twentyseventeen-panel-title">' . sprintf( __( 'Panel %1$s Placeholder', 'twentyseventeen' ), esc_attr( $twentyseventeencounter ) ) . '</span></article>';
				}
				$twentyseventeencounter++;
			}
    }

    $this->post->frontpage_featured_image = get_the_post_thumbnail($this->post->ID, null, array(
      'class' => 'h-auto w-100 absolute bottom-0'
    ));

    // Add it all to the timber context
    $this->timber->addContext( array(
      'panels' => $panels,
    ) );

    // Get the parent context (page, single (post thumbnails etc..))
    return parent::get();
  }
}
