<?php

namespace Models;

use Includes\Classes\CMB2 as CMB2;

class Products extends Page {

  /**
  * __construct
  * @param array $args Model arguments
  */
  public function __construct( $args ) {
      parent::__construct( $args );
  }

  public function get() {

    // Get the product
    $this->args['query']['post_type'] = 'product';

    $productArchive = new \Controllers\Archive( array(
      'query' => $this->args['query']
    ));
    $productsPosts = $productArchive->returnData('archivePosts');

    // Add it all to the timber context
    $this->timber->addContext( array(
      'productPosts' => $productsPosts,
    ) );

    // Get the parent context (page, single (post thumbnails etc..))
    return parent::get();
  }
}
