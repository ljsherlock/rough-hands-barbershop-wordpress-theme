<?php

namespace Models;

use Includes\Classes\CMB2 as CMB2;

class Single extends Base {

  public $sidebars = array();

  public $postDataCallback = null;

  public $relatedPosts = array();

  public $relatedPostsNotIn = array();

  /**
  *   @method __construct
  *   @return get
  **/
  public function __construct($args) {
      parent::__construct($args);

      // Set Sidebars and merge
      $this->sidebars = array(
          'sidebar',
          'sidebar__header',
          'sidebar__footer',
          'sidebar__homepage_main',
          'sidebar__after_app',
      );

      if ( isset( $this->args['sidebars'] ) ) {
          array_merge( $this->sidebars, $this->args['sidebars'] ) ;
      }

      // Accept slug and id as parameters for getting the Post Obj
      if (isset( $this->args['slug'] )) {
        $this->post = new \TimberPost( $this->args['slug'] );
      } else if( isset($this->args['id']) ) {
        $this->post = new \TimberPost( $this->args['id'] );
      } else {
        $this->post = new \TimberPost();
      }

      // Early check for is_single (for use in Twig templates).
      $this->addToObj($this->post, 'is_single', function($post) {
          return is_single($post->ID);
      });
  }

  /**
  *   @method get
  *   @return parent::get()
  *
  *
  **/
  public function get() {
    // Add all to context
    $this->post->featured_image = get_the_post_thumbnail($this->post->ID, null, array(
      'class' => 'h-auto'
    ));

    // Add sidebars to context
    $this->addSidebar($this->sidebars);

    $this->timber->addContext( array(
        'post' => $this->post,
        'standalone' => $this->data['args']['standalone'],
    ));

    return parent::get();
  }
}
