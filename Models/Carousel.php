<?php

namespace Models;

use Includes\Classes\CMB2 as CMB2;

class Carousel extends Page {

  /**
  * __construct
  * @param array $args Model arguments
  */
  public function __construct( $args ) {
      parent::__construct( $args );
  }

  public function get() {

    // Get the slides
    $proudSlider = get_post_meta( $this->post->ID, CMB2::$prefix . 'proudAtWhatWeDoSlider', true );

    // Add it all to the timber context
    $this->timber->addContext( array(
      'proudAtWhatWeDoSliderBlocks' => $proudSlider,
    ) );

    // Get the parent context (page, single (post thumbnails etc..))
    return parent::get();
  }
}
