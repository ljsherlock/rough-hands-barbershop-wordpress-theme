<?php

namespace Models;

use Includes\Classes\CMB2 as CMB2;

class Archive extends Page
{
    /**
    *   @property Array $args archive query
    */
    public $args = array();

    public $term = '';

    public $post = null;

    public $posts = null;

    public $afterQueryCallback = null;

    public $customThumbnailClasses = '';

    /**
    * @method __construct
    *
    * @param Array $args Model arguments
    */
    public function __construct($args)
    {
      parent::__construct($args);

      /*
      *  If a custom query has not been created, populate with query_var
      */
      (!empty($this->args['query']['posts_per_page'])) ?: $this->args['query']['posts_per_page'] = get_query_var('posts_per_page');
      (!empty($this->args['query']['paged'])) ?: $this->args['query']['paged'] = get_query_var('paged');
      (!empty($this->args['query']['taxonomy'])) ?: $this->args['query']['taxonomy'] = get_query_var('taxonomy');
      (!empty($this->args['query']['term'])) ?: $this->args['query']['term'] = get_query_var('term');
      (!empty($this->args['query']['tag'])) ?: $this->args['query']['tag'] = get_query_var('tag');
      (!empty($this->args['query']['category_name'])) ?: $this->args['query']['category_name'] = get_query_var('category_name');
      (!empty($this->args['query']['post_type'])) ?: $this->args['query']['post_type'] = get_query_var('post_type');
      $this->args['query']['orderby'] = 'date';
      $this->args['query']['order'] = 'DESC';
      $this->args['query']['post_status'] = 'publish';


      $this->posts = null;
    }

    /**
    * @method get returns data to the controller
    *
    * @param void
    *
    * @return $context array( $posts, $pagination )
    */
    public function get () {
      $this->addPostsData();

      $this->buildQuery();

      $this->posts['postsArray'] = $this->query($this->args);

      array_walk($this->posts['postsArray'], function(&$post, $key) {
        $post->featured_image = get_the_post_thumbnail($post->ID, null, array(
          'class' => 'h-auto w-100 ' .  $this->customThumbnailClasses
        ));
      });

			$this->timber->addContext(array(
				'archivePosts' => $this->posts,
				'post' => $this->post,
			));

      return parent::get();
    }

    /**
    * @method Add extra data to Posts Array
    *
    * @return NULL
    */
    public function addPostsData () {
      $this->posts['post_type_obj'] = get_post_type_object( $this->args['query']['post_type'] );
      $this->posts['taxonomy_obj'] = get_taxonomy( $this->args['query']['taxonomy'] );
      $this->posts['term_obj'] = new \TimberTerm($this->args['query']['term'], $this->args['query']['taxonomy'] );
      $this->posts['category_term_obj'] = new \TimberTerm($this->args['query']['category_name'], 'category' );
      $this->posts['tag_term_obj'] = new \TimberTerm($this->args['query']['tag'], 'post_tag' );
      $this->posts['posts_per_page'] = $this->args['query']['posts_per_page'];
      if ( isset($this->args['query']['ajax_posts_per_page']) ) {
        $this->posts['ajax_posts_per_page'] = $this->args['query']['ajax_posts_per_page'];
      }

    }

    /**
    * @method Build the query
    *
    * @return NULL
    */
    public function buildQuery() {
      /*
      ** I think a lot of the below code here is overriding the above.
      */
      if( !empty($this->posts['post_type_obj']->name) ){
        $this->args['query']['post_type'] = $this->posts['post_type_obj']->name;
      }
      if( !empty($this->posts->category_term_obj->name) ) {
          $this->args['query']['category_name'] = $this->posts['category_term_obj']->name;
      }
      if( !empty( $this->posts['tag_term_obj']->name) ) {
          $this->args['query']['tax_query'] = array(
            array(
              'taxonomy' => 'post_tag',
              'field' => 'slug',
              'terms' => $this->posts['tag_term_obj']->name
            )
          );
      }
      if( !empty( $this->posts['taxonomy_obj']->name ) && empty( $this->args['query']['tax_query'] ) ) {
          $this->args['query']['tax_query'] = array(
              array( 'field' => 'slug' )
          );
          $this->args['query']['tax_query'][0]['taxonomy'] = $this->posts['taxonomy_obj']->name;
      }
      if( !empty( $this->posts['term_obj']->name ) ) {
          $this->args['query']['tax_query'][0]['terms'] = $this->posts['term_obj']->slug;
      }
    }
}
