<?php

namespace Models;

class Widget__Mailchimp_Subscribe extends Widget {
  public function get () {

    // die(var_dump($this->widget));

    $this->timber->addContext (array(
      'title' => $this->widget->title,
      'buttonText' => $this->widget->buttonText,
      'actionAttr' => $this->widget->actionAttr,
      'legal' => $this->widget->legal
    ));

    return parent::get();
  }
}
